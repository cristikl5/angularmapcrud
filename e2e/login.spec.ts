let loginRef = require('./page-objects/login.po.ts');
let homeRef = require('./page-objects/home.po.ts');

describe('Crud Map App', function () {
  beforeEach(() => {
    browser.get('http://localhost:4200/');
  });

  it('should check if validators errors work', function () {
    loginRef.clickLogin();
    browser.sleep(2000);
  });

  it('should fill username field', function () {
    loginRef.fillUsername();
    loginRef.clickLogin();
    browser.sleep(2000);
  });

  it('should fill password field', function () {
    loginRef.fillPassword();
    loginRef.clickLogin();
    browser.sleep(2000);
  });

  // should work tests

  // it('should login', function () {
  //   browser.get('http://localhost:4200/');
  //   loginRef.loginToHome('admin');
  //   loginRef.clickLogin();
  // });
  //
  // it('should go to navbar', function () {
  //   homeRef.adminAccess();
  // });
  //
  // it('should open modal', function () {
  //   adminRef.openModal();
  // });
  //
  // it('should fill the fields', function () {
  //   adminRef.fillCity('Chisinau');
  //   adminRef.fillStreet('str avicena');
  //   adminRef.fillEmail('hellothere@gmail.com');
  // });
  //
  // it('should add store', function () {
  //   adminRef.addStore();
  //   expect(adminRef.storeList().count()).toEqual(7);
  // });
  //
  // it('should delete the store', () => {
  //   adminRef.deleteStore();
  //   expect(adminRef.storeList().count()).toEqual(6);
  // });
});
