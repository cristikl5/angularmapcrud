let adminRef = require('./page-objects/admin.po.ts');
let loginPass = require('./page-objects/login.po.ts');
let home = require('./page-objects/home.po.ts');

describe('Admin Page', function () {
  it('should check if button is disabled ', function () {
    browser.get('http://localhost:4200/');
    loginPass.loginToHome('admin');
    loginPass.clickLogin();
    home.adminAccess();
  });

  it('should check validation for city field', function () {
    adminRef.openModal();
    adminRef.fillCity('d');
    adminRef.clearCity();
    adminRef.clickStreet();
  });

  it('should check validation for city field', function () {
    adminRef.fillStreet('d');
    adminRef.clearCity();
    adminRef.clickStreet();
  });

  it('should check validation for street field', function () {
    adminRef.fillStreet('d');
    adminRef.clearStreet();
    adminRef.clickEmail();
  });

  it('should check validation for email field', function () {
    adminRef.fillEmail('d');
    adminRef.clearEmail();
    adminRef.clickCity();
  });

  it('should check phone pipe', function () {
    expect(adminRef.checkPhonePipe('058648614')).toContain('+373');
  });

  it('should check card pipe', function () {
    expect(adminRef.checkCardPipe('Visa')).toContain('**** ****');
    expect(adminRef.checkCardPipe('Amex')).toContain('**** ****** *');
    expect(adminRef.checkCardPipe('Diners')).toContain('**** ******');
  });
});
