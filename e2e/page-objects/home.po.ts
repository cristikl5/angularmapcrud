let homePage = function () {
  const navbarAdmin = element(by.id('admin'));

  this.adminAccess = () => {
    navbarAdmin.click();
  };
};

module.exports = new homePage();
