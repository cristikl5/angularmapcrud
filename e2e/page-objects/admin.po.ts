const admin = function () {
  const addStoreModal = element(by.id('addStoreModal'));
  const cityField = element(by.id('city'));
  const streetField = element(by.id('street'));
  const emailField = element(by.id('email'));
  const addStore = element(by.id('addStore'));
  const storesList = element.all(by.id('storeColumnCity'));
  const deleteButton = $$('.btn-danger');
  const phoneNumbers = $$('.phoneNumberPipe');
  const cardNumbers = $$('.cardDetails');

  this.storeList = () => {
    return storesList;
  };

  this.openModal = () => {
    addStoreModal.click();
  };

  this.fillCity = (value) => {
    cityField.sendKeys(value);
  };

  this.clickCity = () => {
    cityField.click();
  };

  this.clearCity = () => {
    cityField.clear();
  };

  this.fillStreet = (value) => {
    streetField.sendKeys(value);
  };

  this.clickStreet = () => {
    streetField.click();
  };

  this.clearStreet = () => {
    streetField.clear();
  };

  this.fillEmail = (value) => {
    emailField.sendKeys(value);
  };

  this.clickEmail = () => {
    emailField.click();
  };

  this.clearEmail = () => {
    emailField.clear();
  };

  this.addStore = () => {
    addStore.click();
  };

  this.deleteStore = () => {
    deleteButton.first().click();
  };

  this.checkPhonePipe = (value) => {
    if (value.charAt(0) === '0') {
      return '+373 45 852 812';
    } else {
      return '+373';
    }
  };

  this.checkCardPipe = function (cardType) {
    switch (cardType) {
      case 'Visa':
        return '**** **** **** 1324';
      case 'Amex':
        return '**** ****** *1132';
      case 'Diners':
        return '**** ****** 4568';
      default:
        return;
    }
  };
};

module.exports = new admin();
