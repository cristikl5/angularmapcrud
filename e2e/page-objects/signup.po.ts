let signupPo = function () {
  const signUpPage = element(by.id('signUpPage'));
  const firstName = element(by.id('firstName'));
  const lastName = element(by.id('lastName'));
  const email = element(by.id('email'));
  const confirmEmail = element(by.id('confirmEmail'));
  const password = element(by.id('password'));
  const confirmPassword = element(by.id('confirmPassword'));
  const submit = element(by.id('submitRegister'));
  const invalidClass = element(by.css('.is-invalid'));

  this.getInvalidClass = function () {
    return invalidClass;
  };

  this.goToSignUp = function () {
    signUpPage.click();
  };

  this.fillEmail = function () {
    email.sendKeys('dasdasdasd@gmail.com');
  };

  this.fillConfirmEmail = function () {
    confirmEmail.sendKeys('dasdasd@gmail.com');
  };

  this.fillPassword = function () {
    password.sendKeys('123456');
  };

  this.fillConfirmPassword = function () {
    confirmPassword.sendKeys('1324');
  };

  this.fillForm = function () {
    firstName.sendKeys('dasdasdad');
    lastName.sendKeys('dasda');
    email.sendKeys('dasdas@gmail.com');
    confirmEmail.sendKeys('dasdas@gmail.com');
    password.sendKeys('123456');
    confirmPassword.sendKeys('123456');
  };

  this.submitForm = function () {
    submit.click();
  };
};

module.exports = new signupPo();
