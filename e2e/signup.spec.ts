let signUpRef = require('./page-objects/signup.po.ts');

describe('Crud Map App', () => {
  beforeEach(() => {
    browser.get('http://localhost:4200/signup');
  });

  it('should check if forms have validation errors', function () {
    signUpRef.submitForm();
    browser.sleep(1000);
  });

  it('should match emails', function () {
    signUpRef.fillEmail();
    signUpRef.fillConfirmEmail();
    signUpRef.submitForm();
    expect(signUpRef.getInvalidClass()).toBeTruthy();
    browser.sleep(2000);
  });

  it('should match passwords', function () {
    signUpRef.fillPassword();
    signUpRef.fillConfirmPassword();
    signUpRef.submitForm();
    expect(signUpRef.getInvalidClass()).toBeTruthy();
    browser.sleep(2000);
  });

  it('should fill the fields', function () {
    signUpRef.fillForm();
    signUpRef.submitForm();
    browser.sleep(2000);
  });
});
