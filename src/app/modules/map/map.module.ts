import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { MapComponent } from '../../map/map.component';
import { SharedModule } from '../../shared/shared.module';
import { MapRoutingModule } from './map-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [MapComponent],
  imports: [
    SharedModule,
    MapRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDC2JId8u9K3GGTX4KbavWTqSWt7UGnIDg',
    }),
    FormsModule,
  ],
})
export class MapModule {}
