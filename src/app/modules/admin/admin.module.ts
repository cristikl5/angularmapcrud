import { NgModule } from '@angular/core';
import { AdminDashboardComponent } from '../../dashboard/admin-dashboard.component';
import { SharedModule } from '../../shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AddModalComponent } from '../../modals/add-modal/add-modal.component';
import { EditModalComponent } from '../../modals/edit-modal/edit-modal.component';
import { InfoModalComponent } from '../../modals/info-modal/info-modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NumberFormatterPipe } from '../../custom-pipes/number-formatter.pipe';
import { CardPipe } from '../../custom-pipes/card.pipe';
import { PhoneMaskDirective } from '../../phone-mask.directive';
import { IConfig, NgxMaskModule } from 'ngx-mask';

const maskConfig: Partial<IConfig> = {
  validation: true,
};

@NgModule({
  declarations: [
    AdminDashboardComponent,
    AddModalComponent,
    EditModalComponent,
    InfoModalComponent,
    NumberFormatterPipe,
    CardPipe,
    PhoneMaskDirective,
  ],
  imports: [
    SharedModule,
    AdminRoutingModule,
    NgbModule,
    NgxMaskModule.forRoot(maskConfig),
  ],
  entryComponents: [AddModalComponent, EditModalComponent, InfoModalComponent],
})
export class AdminModule {}
