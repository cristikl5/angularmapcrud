import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './sessions/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { MapModule } from './modules/map/map.module';
import { SharedModule } from './shared/shared.module';
import { HomeComponent } from './home/home.component';
import { SignUpComponent } from './sessions/sign-up/sign-up.component';
import { HasRoleDirective } from './directives/has-role.directive';
import { NavbarComponent } from './navbar/navbar.component';
import { ExternalLinkDirective } from './directives/external-link.directive';
import { StepsComponent } from './sessions/sign-up/steps/steps.component';
import { StepTemplateComponent } from './sessions/sign-up/step-template/step-template.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    SignUpComponent,
    HasRoleDirective,
    NavbarComponent,
    ExternalLinkDirective,
    StepsComponent,
    StepTemplateComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    MapModule,
  ],
  providers: [AuthService],
  bootstrap: [AppComponent],
})
export class AppModule {}
