import { Component, OnInit } from '@angular/core';
import { LocationsService } from '../services/locations.service';
import { Marker } from '../models/Marker';
import { MouseEvent } from '@agm/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  lat: number = 40.7127281;
  lng: number = -74.0060152;
  zoom = 4;
  markers: Marker[];
  markerCityName = new FormControl('', { updateOn: 'blur' });
  markerEmail = new FormControl('', { updateOn: 'blur' });
  filteredMarkers: Marker[];
  filteredEmails: Marker[];

  constructor(private locationService: LocationsService) {}

  ngOnInit() {
    this.getMarkers();
    this.markerCityName.valueChanges.subscribe((res) => {
      this.filterStores(res);
    });
    this.markerEmail.valueChanges.subscribe((res) => {
      this.filterEmailByMarker(res);
    });
  }

  getMarkers() {
    this.locationService.getMarkers().subscribe((data: Marker[]) => {
      this.markers = data;
      this.filteredMarkers = this.markers;
    });
  }

  dragEnd($event: MouseEvent) {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;
  }

  filterStores(value) {
    if (!value) {
      this.filteredMarkers = this.markers;
    }
    value = value.toLowerCase();
    this.filteredMarkers = this.markers.filter((marker) =>
      marker.city.toLocaleLowerCase().includes(value)
    );
  }

  filterEmailByMarker(value) {
    if (!value) {
      this.filteredEmails = this.filteredMarkers;
    }
    value = value.toLowerCase();
    this.filteredMarkers = this.markers.filter((marker) =>
      marker.email.toLocaleLowerCase().includes(value)
    );
  }
}
