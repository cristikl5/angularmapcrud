import { Component, OnInit } from '@angular/core';
import { LocationsService } from '../services/locations.service';
import { Marker } from '../models/Marker';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddModalComponent } from '../modals/add-modal/add-modal.component';
import { EditModalComponent } from '../modals/edit-modal/edit-modal.component';
import { InfoModalComponent } from '../modals/info-modal/info-modal.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css'],
})
export class AdminDashboardComponent implements OnInit {
  storeForm: FormGroup;
  stores: Marker[];

  constructor(
    private locationService: LocationsService,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.getStores();
    this.form();
  }

  getStores() {
    this.locationService.getMarkers().subscribe((data: any) => {
      this.stores = data;
    });
  }

  form() {
    this.storeForm = this.fb.group({
      city: ['', [Validators.required, Validators.minLength(4)]],
      street: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', [Validators.required, Validators.email]],
    });
  }

  openEditModal(store) {
    this.storeForm.controls.city.setValue(store.city);
    this.storeForm.controls.street.setValue(store.street);
    this.storeForm.controls.email.setValue(store.email);
    const modalRef = this.modalService.open(EditModalComponent, {
      ariaLabelledBy: 'modal-basic-title',
    });
    modalRef.componentInstance.storeForm = this.storeForm;
    modalRef.componentInstance.store = store;
    modalRef.componentInstance.stores = this.stores;
  }

  openAddModal() {
    this.storeForm.reset();
    const modalRef = this.modalService.open(AddModalComponent, {
      ariaLabelledBy: 'modal-basic-title',
    });
    modalRef.componentInstance.storeForm = this.storeForm;
    modalRef.componentInstance.stores = this.stores;
  }

  openInfoModal(store) {
    const modalRef = this.modalService.open(InfoModalComponent, {
      ariaLabelledBy: 'modal-basic-title',
    });
    modalRef.componentInstance.store = store;
  }

  deleteStore(storeId) {
    this.locationService.deleteStore(storeId).subscribe(() => {
      this.stores = this.stores.filter((store) => storeId !== store.id);
    });
  }
}
