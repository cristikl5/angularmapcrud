export interface Marker {
  id: number;
  lat: number;
  lng: number;
  draggable: boolean;
  label?: string;
  street?: string;
  icon?: string;
  city?: string;
  state?: string;
  email?: string;
  number?: string;
  card?: string;
  cardType?: string;
}
