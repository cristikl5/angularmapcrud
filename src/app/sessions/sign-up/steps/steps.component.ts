import { Component, OnInit } from '@angular/core';
import { StepsService } from '../../../services/steps.service';
import { Observable } from 'rxjs';
import { StepModel } from '../../../models/StepModel';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss'],
})
export class StepsComponent implements OnInit {
  steps: Observable<StepModel[]>;
  currentStep: Observable<StepModel>;

  constructor(private stepsService: StepsService) {}

  ngOnInit(): void {
    this.steps = this.stepsService.getSteps();
    this.currentStep = this.stepsService.getCurrentStep();
  }

  onStepClick(step): void {
    this.stepsService.setCurrentStep(step);
  }
}
