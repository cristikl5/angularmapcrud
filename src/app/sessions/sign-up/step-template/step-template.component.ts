import { Component, Input, OnInit } from '@angular/core';
import { StepModel } from '../../../models/StepModel';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-step-template',
  templateUrl: './step-template.component.html',
  styleUrls: ['./step-template.component.scss'],
})
export class StepTemplateComponent implements OnInit {
  @Input() step: StepModel;
  password = new FormControl('');

  constructor() {}

  ngOnInit(): void {}

  onComplete(): void {
    this.step.isComplete = true;
  }
}
