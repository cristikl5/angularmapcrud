import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StepsService } from '../../services/steps.service';
import { StepModel } from '../../models/StepModel';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
  currentStep: StepModel;
  currentStepSub: Subscription;
  signUpForm: FormGroup;
  passwordGroup: FormGroup;

  constructor(private fb: FormBuilder, private stepService: StepsService) {}

  ngOnInit(): void {
    this.createForm();
    this.passwordForm();
    this.currentStepSub = this.stepService
      .getCurrentStep()
      .subscribe((step: StepModel) => {
        this.currentStep = step;
      });
  }

  createForm() {
    this.signUpForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(4)]],
      lastName: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', [Validators.required, Validators.email]],
    });
  }

  passwordForm() {
    this.passwordGroup = this.fb.group({
      password: ['', Validators.required, Validators.minLength(6)],
    });
  }

  goNext() {
    if (this.stepService.isLastStep()) {
      this.stepService.moveToNextStep();
    }
  }

  showButtonLabel() {
    return !this.stepService.isLastStep() ? 'Next' : null;
  }
}
