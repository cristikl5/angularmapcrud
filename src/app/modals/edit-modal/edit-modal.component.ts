import { Component, Input, OnInit } from '@angular/core';
import { LocationsService } from '../../services/locations.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Marker } from '../../models/Marker';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.css'],
})
export class EditModalComponent implements OnInit {
  @Input() storeForm;
  @Input() store: Marker;
  @Input() stores: Marker[];

  constructor(
    private locationService: LocationsService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {}

  get f() {
    return this.storeForm.controls;
  }

  get city() {
    return this.storeForm.get('city');
  }

  get street() {
    return this.storeForm.get('street');
  }

  get email() {
    return this.storeForm.get('email');
  }

  updateStore(store) {
    this.locationService
      .updateStore(store.id, this.storeForm.value)
      .subscribe((data: any) => {
        const index = this.stores.indexOf(store);
        this.stores[index] = data;
        this.modalService.dismissAll();
      });
  }

  modalClose() {
    this.modalService.dismissAll();
  }
}
