import { Component, Input, OnInit } from '@angular/core';
import { Marker } from '../../models/Marker';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-info-modal',
  templateUrl: './info-modal.component.html',
  styleUrls: ['./info-modal.component.css'],
})
export class InfoModalComponent implements OnInit {
  @Input() store: Marker;
  constructor(private modalService: NgbModal) {}

  ngOnInit(): void {}

  modalClose() {
    this.modalService.dismissAll();
  }
}
