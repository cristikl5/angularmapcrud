import { Component, Input, OnInit } from '@angular/core';
import { LocationsService } from '../../services/locations.service';
import { Marker } from '../../models/Marker';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-modal',
  templateUrl: './add-modal.component.html',
  styleUrls: ['./add-modal.component.css'],
})
export class AddModalComponent implements OnInit {
  storeForm: FormGroup;
  @Input() stores: Marker[];
  submitted = false;
  public mask = [
    '(',
    /[1-9]/,
    /\d/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
  ];

  constructor(
    private locationService: LocationsService,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.storeForm = this.fb.group({
      city: ['', [Validators.required, Validators.minLength(4)]],
      street: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', Validators.required],
      cardNumber: ['', Validators.required],
    });
  }

  addStore() {
    this.submitted = true;
    if (this.storeForm.valid) {
      this.locationService.addStore(this.storeForm.value).subscribe((res) => {
        this.stores.unshift(res);
        this.modalService.dismissAll();
      });
    }
  }

  modalClose() {
    this.modalService.dismissAll();
  }
}
