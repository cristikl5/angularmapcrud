import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from './guards/auth-guard.service';
import { LoginComponent } from './sessions/login/login.component';
import { AuthService } from './services/auth.service';
import { HomeComponent } from './home/home.component';
import { SignUpComponent } from './sessions/sign-up/sign-up.component';
import { HasRoleGuard } from './guards/has-role.guard';

const adminRoutes: Routes = [
  {
    path: 'map',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/map/map.module').then((m) => m.MapModule),
  },
  {
    path: 'admin',
    canActivate: [HasRoleGuard],
    data: { role: 'admin' },
    loadChildren: () =>
      import('./modules/admin/admin.module').then((m) => m.AdminModule),
  },
];

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'signup',
    component: SignUpComponent,
  },
  ...adminRoutes,
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard, AuthService],
})
export class AppRoutingModule {}
