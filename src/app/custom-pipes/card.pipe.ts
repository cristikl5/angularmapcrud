import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cardPipe',
})
export class CardPipe implements PipeTransform {
  transform(cardNumber: string, cardType: string) {
    const visaAndMasterSection = cardNumber.replace(
      cardNumber.substr(0, 12),
      '**** **** **** '
    );
    const amexSection = cardNumber.replace(
      cardNumber.substr(0, 11),
      '**** ****** *'
    );
    const dinersSection = cardNumber.replace(
      cardNumber.substr(0, 10),
      '**** ****** '
    );
    switch (cardType) {
      case 'Visa':
        return `${visaAndMasterSection}`;
      case 'MasterCard':
        return `${visaAndMasterSection}`;
      case 'Amex':
        return `${amexSection}`;
      case 'Diners':
        return `${dinersSection}`;
      default:
        return;
    }
  }
}
