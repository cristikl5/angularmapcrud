import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberFormatter',
})
export class NumberFormatterPipe implements PipeTransform {
  transform(value: string) {
    const countryCode = '+373';
    const areaCode = value.substr(1, 2);
    const middleSection = value.substr(3, 3);
    const endSection = value.substr(6);

    if (value.charAt(0) === '0') {
      return `${countryCode} ${areaCode} ${middleSection} ${endSection}`;
    } else {
      return `${value.substr(0, 4)} ${value.substr(4, 2)} ${value.substr(
        6,
        3
      )} ${value.substr(9)}`;
    }
  }
}
