import { Injectable } from '@angular/core';
import { StepModel } from '../models/StepModel';
import { BehaviorSubject, Observable } from 'rxjs';

const STEPS = [
  { stepIndex: 1, isComplete: false },
  { stepIndex: 2, isComplete: false },
  { stepIndex: 3, isComplete: false },
];

@Injectable({
  providedIn: 'root',
})
export class StepsService {
  steps$: BehaviorSubject<StepModel[]> = new BehaviorSubject<StepModel[]>(
    STEPS
  );
  currentStep$: BehaviorSubject<StepModel> = new BehaviorSubject<StepModel>(
    null
  );

  constructor() {
    this.currentStep$.next(this.steps$.value[0]);
  }

  setCurrentStep(step: StepModel): void {
    this.currentStep$.next(step);
  }
  getSteps(): Observable<StepModel[]> {
    return this.steps$.asObservable();
  }

  getCurrentStep(): Observable<StepModel> {
    return this.currentStep$.asObservable();
  }

  moveToNextStep(): void {
    const index = this.currentStep$.value.stepIndex;
    if (index < this.steps$.value.length) {
      this.currentStep$.next(this.steps$.value[index]);
    }
  }

  isLastStep(): boolean {
    return this.currentStep$.value.stepIndex === this.steps$.value.length;
  }
}
