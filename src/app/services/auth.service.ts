import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private router: Router) {}

  login(password) {
    localStorage.setItem('token', 'dasdasda');
    if (password.includes('admin')) {
      localStorage.setItem('role', 'admin');
    } else {
      localStorage.setItem('role', 'user');
    }
    this.router.navigate(['/']);
  }

  logOut() {
    localStorage.clear();
    this.router.navigate(['login']);
  }

  isLoggedIn() {
    const token = localStorage.getItem('token');
    return !!token;
  }
}
