import { Injectable } from '@angular/core';
import { Marker } from '../models/Marker';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseURL = 'http://localhost:3000/markers';

@Injectable({
  providedIn: 'root',
})
export class LocationsService {
  constructor(private httpClient: HttpClient) {}

  getMarkers(): Observable<Marker[]> {
    return this.httpClient.get<Marker[]>(baseURL);
  }

  deleteStore(id): Observable<Marker> {
    return this.httpClient.delete<Marker>(`${baseURL}/${id}`);
  }

  addStore(store): Observable<Marker> {
    return this.httpClient.post<Marker>(baseURL, store);
  }

  updateStore(storeId, updatedStore: Marker) {
    return this.httpClient.put(`${baseURL}/${storeId}`, updatedStore);
  }
}
