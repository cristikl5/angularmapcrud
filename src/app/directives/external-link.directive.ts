import { Directive, ElementRef, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appExternalLink]',
})
export class ExternalLinkDirective implements OnInit {
  icon = 'https://img.icons8.com/material-outlined/15/000000/external-link.png';

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  ngOnInit() {
    this.el.nativeElement.innerHTML += `<img src="${this.icon}">`;
  }
}
